///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Andrew <chaoran@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   04-13-21
///////////////////////////////////////////////////////////////////////////////
#include<iostream>
#include "list.hpp"

const bool DoubleLinkedList::empty() const {
   return head == nullptr;
}

void DoubleLinkedList::push_front(Node* newNode) {
   if (newNode == nullptr) {
      return;
   }
   if (head != nullptr) {
      newNode->next = head;
      newNode->prev = nullptr; //
      head->prev = newNode; //
      head = newNode;
//      count++;
   } else {
      newNode->prev = nullptr; //
      newNode->next = nullptr; //
      head = newNode; //
      tail = newNode; //
   }
   count++;
}

void DoubleLinkedList::push_back(Node* newNode) {
    if (newNode == nullptr) {
        return;
    }
    if (tail != nullptr) {
      newNode->next = nullptr;
      newNode->prev = tail;
      tail->next = newNode;
      tail = newNode;
    } else {
      newNode->prev = nullptr;//
      newNode->next = nullptr;//
      head = newNode;//
      tail = newNode; //
    }
   count++;
}

Node* DoubleLinkedList::get_first() const {
   return head;
}

Node* DoubleLinkedList::get_last() const {
   return tail;
}


Node* DoubleLinkedList::pop_front() {
    if (head == nullptr) {
        return nullptr;
    }
    if (head == tail) {
      Node* returnValue = head;
      head = nullptr;
      tail = nullptr;
      count--;
      return returnValue;
    } else {
      Node* returnValue = head;
      head = head->next;
      returnValue->next = nullptr;
      count--;
      return returnValue;
   }
}

Node* DoubleLinkedList::pop_back() {
    if (tail == nullptr) {
        return nullptr;
    }
    if (head == tail) {
      Node* returnValue = tail;
      head = nullptr;
      tail = nullptr;
      count--;
      return returnValue;
    } else {
      Node* returnValue = tail;
      head = head->prev;
      returnValue->prev = nullptr;
      count--;
      return returnValue;
   }

}

Node* DoubleLinkedList::get_next( const Node* currentNode ) const {
   return currentNode->next;
}

Node* DoubleLinkedList::get_prev( const Node* currentNode) const {
   return currentNode->prev;
}

/*
unsigned int DoubleLinkedList::size() const {
		Node* temp = head;
		int count = 0;
		while (temp != nullptr) {
			count++;
		return count;
}
*/
//==================================
// insert before and after/isin
//==================================

const bool DoubleLinkedList::isIn(Node* aNode) const {
   Node* currentNode = head;
   while (currentNode != nullptr) {
      if (aNode == currentNode) {
         return true;
      }
      currentNode = currentNode->next;
   }
   return false;
}

void DoubleLinkedList::insert_after( Node* currentNode, Node* newNode ) {
   if (currentNode == nullptr && head == nullptr) {
      push_front( newNode );
      return;
   }
   if (currentNode != nullptr && head == nullptr) {
      assert(false);
   }
   if (currentNode == nullptr && head != nullptr) {
      assert(false);
   }
   assert(currentNode != nullptr && head == nullptr);
   assert(isIn(currentNode));
   assert(!isIn(newNode));
   if (tail != currentNode) {
      newNode->next = currentNode->next;
      currentNode->next = newNode;
      newNode->prev = currentNode;
      newNode->next->prev = newNode;
   } else {
      push_back(newNode);
   }

}

void DoubleLinkedList::insert_before (Node* currentNode, Node* newNode) {
   if (currentNode == nullptr && head == nullptr) {
      push_front( newNode );
      return;
   }
   if (currentNode != nullptr && head == nullptr) {
      assert(false);
   }
   if (currentNode == nullptr && head != nullptr) {
      assert(false);
   }
   assert(currentNode != nullptr && head == nullptr);
   assert(isIn(currentNode));
   assert(!isIn(newNode));
   if (head != currentNode) {
      newNode->prev = currentNode->prev;
      currentNode->prev = newNode;
      newNode->next = currentNode;
      newNode->prev->next = newNode;
   } else {
      push_front(newNode);
   }

}
//==========
//Swap (actual pain)
//==========



const bool DoubleLinkedList::isSorted() const {
   if (count <= 1) {
      return true;
   }
   for (Node* i = head; i->next != nullptr; i = i->next) {
      if (*i > *i->next) {
         return false;
      }
   }
   return true;

}

void DoubleLinkedList::swap(Node* node1, Node* node2){
   assert( isIn(node1) && isIn(node2));
//   assert(node1!=nullptr);
//   assert(node2!=nullptr);
   assert(node1!=nullptr && node2!=nullptr);

   if (node1 == node2) {
      return;
   }

//heads

   if ( node1 == head ) {
      node1->prev = node2->prev;
      node2->prev->next = node1;
      if (node2->next != nullptr){
         node2->next->prev = node1;
         node1->next->prev = node2;
         Node* temp = node1->next;//
         node1->next = node2->next;
         node2->next = temp;//
      } else {
         node2->next = node1->next;
         node1->next->prev = node2;
         node1->next = nullptr;
         tail = node1;
      }
      node2->prev = nullptr;
      head = node2;
      return;
   }

   if ( node2 == head ) {
      node2->prev = node1->prev;
      node1->prev->next = node2;
      if (node1->next != nullptr){
         node1->next->prev = node2;
         node2->next->prev = node1;
         Node* temp = node2->next;//
         node2->next = node1->next;
         node1->next = temp;//
      } else {
         node1->next = node2->next;
         node2->next->prev = node1;
         node2->next = nullptr;
         tail = node2;
      }
      node1->prev = nullptr;
      head = node1;
      return;
   }

//tails

   if ( node1 == tail ) {
      node1->next = node2->next;
      node2->next->prev = node1;
      if (node2->prev != nullptr){
         node2->prev->next = node1;
         node1->prev->next = node2;
         Node* temp = node1->prev;
         node1->prev = node2->prev;
         node2->prev = temp;
      }else{
         node2->prev = node1->prev;
         node1->prev->next = node2;
         node1->prev = nullptr;
         head = node1;
      }
      node2->next = nullptr;
      tail = node2;
      return;
   }

   if ( node2 == tail ){
      node2->next = node1->next;
      node1->next->prev = node2;
      if (node1->prev != nullptr) {
         node1->prev->next = node2;
         node2->prev->next = node1;
         Node* temp = node2->prev;
         node2->prev = node1->prev;
         node1->prev = temp;
      } else {
         node1->prev = node2->prev;
         node2->prev->next = node1;
         node2->prev = nullptr;
         head = node2;
      }
      node1->next = nullptr;
      tail = node1;
      return;
   }

   if (node1->next == node2 && node2->prev == node1) {
      node2->prev = node1->prev;
      node1->prev->next = node2;
      node1->prev = node2;
      node1->next = node2->next;
      node2->next->prev = node1;
      node2->next = node1;
   } else {
      Node* temp = node1->next; //
      Node* temp2 = node1->prev; ///
      node1->next = node2->next;
      node2->next->prev = node1;
      node1->prev = node2->prev;
      node2->prev->next = node1;
      node2->next = temp; //
      temp->prev = node2; //
      node2->prev = temp2; ///
      temp2->next = node2; ///
      return;
   }
}

void DoubleLinkedList::insertionSort() {
   Node* currentNode = head;
   Node* nextNode = nullptr;
   Node* firstNode = nullptr;

   while(currentNode != nullptr) {
      firstNode = currentNode;
      nextNode = currentNode->next;
      while(nextNode != nullptr) {
         if (*firstNode > *nextNode) {
            firstNode = nextNode;
         }
         nextNode->next = nextNode;
      }
      swap(currentNode, firstNode);
      firstNode->next = currentNode;
   }

}
