///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Andrew <chaoran@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   04-13-21
///////////////////////////////////////////////////////////////////////////////
#include<iostream>
#include "node.hpp"
#include<cassert>
#pragma once


class DoubleLinkedList {
   public:
      const bool empty() const;
      void push_front( Node* newNode );
      Node* pop_front();
      Node* get_first() const;
      Node* get_next( const Node* currentNode ) const;
//      unsigned int size() const;

      void push_back( Node* newNode );
      Node* pop_back();
      Node* get_last() const;
      Node* get_prev( const Node* currentNode ) const;


      const bool isIn(Node* aNode) const;
      void insert_after( Node* currentNode, Node* newNode );
      void insert_before( Node* currentNode, Node* newNode );

      inline const unsigned int size() const { return count; };

      void swap( Node* node1, Node* node2 );
      void insertionSort();
      const bool isSorted() const;

   protected:
      unsigned int count = 0;
      Node* head = nullptr;
      Node* tail = nullptr;
};
